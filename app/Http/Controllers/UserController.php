<?php
namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index() {
        return response()->json(User::all(), 200);
    }

    public function store(Request $request){
        $user = new User();
        $user->fill($request->all());
        $user->password = bcrypt($request->get('password'));
        $user->save();
        return response()->json($user, 200);
    }

    public function update(Request $request, $id){
        $user = User::find($id);
        $user->fill($request->all());
        $user->save();
        return response()->json($user, 200);
    }

    public function destroy($id){
        User::destroy($id);
        return response()->json([], 200);
    }

    public function show($id){
        $user = User::find($id);
        return response()->json($user, 200);
    }
}